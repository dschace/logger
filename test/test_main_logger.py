import pytest
from Logger.logger import Logger


@pytest.fixture(scope='module')
def setup():
    print('****** Setup ******')

    logger = Logger()
    logger.start_logger('test_task', 'test description for test task', file='test.json', data='test data2')
    yield logger

def test_logger(setup):
    logger = setup

    assert logger.task_title == 'test_task'
    assert logger.task_description == 'test description for test task'
