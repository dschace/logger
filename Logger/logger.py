import json
from datetime import datetime


class Logger(object):

    def __init__(self):
        self.task_title = ""
        self.task_description = ""

    def start_logger(self, task_title, task_description, file, data):
        """
        :param task_title: Robocorp Lab Task Title
        :param task_description: Description of task suite being run
        :param file: String containing path to save log file
        :param data:  Any additional data  desired to be added to logging file
        :return: Calls log_output method and adds data
        """
        self.task_title = task_title
        self.task_description = task_description

        self.log_output(file, data)

    def log_output(self, file, data):
        """
        :param file: String containing file log file path
        :param data: Any additional data desired to be saved to log file
        :return: Reads in any current log data for current digital worker run.  Writes to log file in current run cycle
                 with time stamp and additional data used to run task along with previous data if multiple digital
                 workers are called in succession.
        """
        file_data = []

        try:
            with open (file) as f:
                prev_data = json.load(f)
                file_data = prev_data
        except Exception as e:
            print(e)

        file_data.append({
            'Time Stamp': str(datetime.now()),
            'Task Title': self.task_title,
            'Task Description': self.task_description,
            'Data': data
        })

        with open (file, 'w') as outfile:
            json.dump(file_data, outfile, indent=4, ensure_ascii=False)

