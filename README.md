# Digital Worker Logger

### Direction:

Intention behind this small project was to create a simple, but effective logging library that can be 
installed/imported into a digital worker for logging of the task activities and its input data.  I chose to 
package and upload this library to Gitlab and included the gitlab url within the digital worker's conda.yaml
for inclusion in the digital worker environment.  I chose to keep this code light and void of external libraries
(except for pytest) to ensure a lighter footprint.  

**If I had more time, here a few things I would change/improve upon:**
1. An improved data structure for the log, especially in instances of task definitions such as writing to the
   excel file.
2. Additional test functions and try catch blocks to more robustly test the logger library and catch instances
   where the code might fail due to inputs.
3. An iterative method to take in additional input log data and recursively structure it versus having to build 
   additional input data within the digital worker.
4. Would have made more use of the Robocorp Lab's default variables and included those that provided additional 
   useful log insights as input parameters to logger.

